#!/bin/sh
#
# Copyright (c) 2022  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

set -e

check()
{
	local tempf="$1" loc="$2" c1="$3" c2="$4" c3="$5"
	shift 5

	printf -- '\n==== Checking the result for the %s locale\n\n' "$loc"
	env LC_CTYPE="$loc" "$@" > "$tempf"

	# Yes, there are dozens of ways to make this more generic. I know.
	if ! grep -Fxe "Does it match '?': $c1" -- "$tempf"; then
		echo 'Failed the "?" check' 1>&2
		exit 1
	fi

	if ! grep -Fxe "Does it match '??': $c2" -- "$tempf"; then
		echo 'Failed the "??" check' 1>&2
		exit 1
	fi

	if ! grep -Fxe "Does it match '???': $c3" -- "$tempf"; then
		echo 'Failed the "???" check' 1>&2
		exit 1
	fi
}

if [ "$#" -eq 0 ]; then
	echo 'Usage: run-test.sh command [args...]' 1>&2
	echo '' 1>&2
	echo 'Examples: run-test.sh ./fnmess' 1>&2
	echo '          run-test.sh python3 fnmess.py' 1>&2
	echo '' 1>&2
	exit 1
fi

if [ -z "$FNMESS_TEST_U8LOC" ]; then
	echo 'Looking for an UTF-8-capable locale'
	u8loc="$(locale -a | grep -Eie '\.utf-?8([^a-zA-Z0-9_-]|$)' | head -n1)"
	if [ -z "$u8loc" ]; then
		echo "No UTF-8-capable locale found" 1>&2
		exit 1
	fi
else
	u8loc="$FNMESS_TEST_U8LOC"
fi
echo "Using '$u8loc' as a multibyte locale"

if [ -z "$FNMESS_TEST_SINGLOC" ]; then
	echo 'Looking for an ISO-8859-1 or ISO-8859-15 locale'
	singloc="$(locale -a | grep -Eie '\.iso-?8859-?(1|15)([^a-zA-Z0-9_-]|$)' | head -n1)"
	if [ -z "$singloc" ]; then
		echo "No ISO-8859-1 or ISO-8859-15 locale found" 1>&2
		exit 1
	fi
else
	singloc="$FNMESS_TEST_SINGLOC"
fi
echo "Using '$singloc' as a single-byte locale"

tempf="$(mktemp)"
trap "rm -f -- '$tempf'" EXIT INT HUP QUIT TERM
echo "Using '$tempf' as a temporary file"

printf -- '\n==== Running in the %s locale, expected: no, yes, no\n\n' "$singloc"
env LC_CTYPE="$singloc" "$@"
check "$tempf" "$singloc" 'no' 'yes' 'no' "$@"

printf -- '\n==== Running in the %s locale, expected: yes, no, no\n' "$u8loc"
env LC_CTYPE="$u8loc" "$@"
check "$tempf" "$u8loc" 'yes' 'no' 'no' "$@"

printf -- '\n==== Seems fine!\n\n'
